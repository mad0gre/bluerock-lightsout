package victor.bluerock.lightsout.processor;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import victor.bluerock.lightsout.domain.Coordinate;
import victor.bluerock.lightsout.domain.Sample;
import victor.bluerock.lightsout.domain.Solution;
import victor.bluerock.lightsout.loader.SampleFileLoader;
import victor.bluerock.lightsout.strategy.LightsOutStrategy;
import victor.bluerock.lightsout.strategy.LightsOutStrategyFactory;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SampleProcessorTest {

    @Mock
    private SampleFileLoader loader;

    @Mock
    private LightsOutStrategyFactory strategyFactory;

    @Mock
    private LightsOutStrategy strategy;

    @InjectMocks
    private SampleProcessor processor;

    @Test
    void process_null_allFilesLoadedAndProcessed() {
        Sample sample1 = Sample
                .builder()
                .sampleFile("sample01")
                .depth(2)
                .build();
        Sample sample2 = Sample
                .builder()
                .sampleFile("sample02")
                .depth(3)
                .build();
        List<Sample> samples = Stream
                .of(sample1, sample2)
                .collect(Collectors.toList());

        when(strategyFactory.createStrategies())
                .thenReturn(Collections.singletonList(strategy));

        when(loader.loadFiles())
                .thenReturn(samples);

        processor.process(null);

        verify(loader).loadFiles();
        verify(strategy).findSolution(sample1);
        verify(strategy).findSolution(sample2);
        verifyNoMoreInteractions(loader);
        verifyNoMoreInteractions(strategy);
    }

    @Test
    void process_someFile_fileLoadedAndProcessed() {
        String fileName = "file.txt";

        Sample sample = Sample
                .builder()
                .sampleFile(fileName)
                .depth(2)
                .build();

        List<Coordinate> coordinates = Stream
                .of(Coordinate.builder()
                                .x(1)
                                .y(1)
                                .build(),
                        Coordinate.builder()
                                .x(2)
                                .y(2)
                                .build())
                .collect(Collectors.toList());
        Solution solution = Solution
                .builder()
                .sampleFile(fileName)
                .coordinates(coordinates)
                .build();

        when(strategyFactory.createStrategies())
                .thenReturn(Collections.singletonList(strategy));

        when(loader.loadFile(fileName))
                .thenReturn(sample);

        when(strategy.findSolution(sample))
                .thenReturn(solution);

        processor.process(fileName);

        verify(loader).loadFile(fileName);
        verify(strategy).findSolution(sample);
        verifyNoMoreInteractions(loader);
        verifyNoMoreInteractions(strategy);
    }
}
