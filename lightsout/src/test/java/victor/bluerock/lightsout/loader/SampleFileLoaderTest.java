package victor.bluerock.lightsout.loader;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import victor.bluerock.lightsout.domain.Sample;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SampleFileLoaderTest {

    private SampleFileLoader loader;

    @BeforeEach
    void before() {
        loader = new SampleFileLoader();
    }

    @Test
    void loadFiles_none_sampleCount() {
        List<Sample> samples = loader.loadFiles();

        assertEquals(3, samples.size());
    }

    @Test
    void loadFile_null_null() {
        Sample sample = loader.loadFile(null);

        assertNull(sample);
    }

    @Test
    void loadFile_notExist_null() {
        Sample sample = loader.loadFile("file.wtf");

        assertNull(sample);
    }

    @Test
    void loadFile_exists_correctPuzzle() {
        Sample sample = loader.loadFile("03.txt");

        int[][] expectedPuzzle = new int[][] {
                {2, 1, 2, 1},
                {2, 2, 1, 2},
                {1, 0, 0, 1},
                {2, 0, 1, 1},
                {1, 2, 1, 1},
                {2, 1, 1, 1}
        };

        List<int[][]> expectedPieces = Stream.of(
                new int[][] {
                        {1, 0, 0, 0},
                        {1, 0, 1, 0},
                        {1, 1, 1, 1},
                        {1, 1, 0, 0}
                },
                new int[][] {
                        {1, 1, 1},
                        {1, 0, 1},
                },
                new int[][] {
                        {1, 1},
                        {1, 0}
                },
                new int[][] {
                        {1, 1},
                        {0, 1},
                        {0, 1}
                },
                new int[][] {
                        {1, 1, 0},
                        {0, 1, 1},
                        {0, 1, 0},
                        {0, 1, 0}
                },
                new int[][] {
                        {1, 1, 0, 0},
                        {0, 1, 1, 1}
                },
                new int[][] {
                        {0, 1, 0},
                        {0, 1, 0},
                        {1, 1, 0},
                        {1, 1, 1}
                },
                new int[][] {
                        {0, 0, 1, 1},
                        {0, 0, 1, 1},
                        {1, 1, 1, 0}
                },
                new int[][] {
                        {0, 1, 0, 0},
                        {0, 1, 1, 0},
                        {1, 0, 1, 0},
                        {1, 1, 1, 1}
                },
                new int[][] {
                        {1},
                        {1},
                        {1},
                        {1}
                },
                new int[][] {
                        {0, 1, 0},
                        {0, 1, 1},
                        {0, 1, 0},
                        {0, 1, 0},
                        {1, 1, 1}
                })
                .collect(Collectors.toList());

        assertEquals("03.txt", sample.getSampleFile());
        assertEquals(3, sample.getDepth());
        assertTrue(Arrays.deepEquals(expectedPuzzle, sample.getPuzzle()));
        assertTrue(Arrays.deepEquals(expectedPieces.toArray(new int[][][] {}), sample.getPieces().toArray(new int[][][] {})));
    }
}
