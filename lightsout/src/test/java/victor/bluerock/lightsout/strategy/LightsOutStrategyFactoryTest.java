package victor.bluerock.lightsout.strategy;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import victor.bluerock.lightsout.strategy.impl.NaiveStrategy;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LightsOutStrategyFactoryTest {

    private LightsOutStrategyFactory factory;

    @BeforeEach
    void before() {
        factory = new LightsOutStrategyFactory();
    }

    @Test
    void createStrategies_none_correctStrategies() {
        List<LightsOutStrategy> strategies = factory.createStrategies();

        assertEquals(1, strategies.size());
        assertEquals(NaiveStrategy.class, strategies.get(strategies.size() - 1).getClass());
    }
}
