package victor.bluerock.lightsout.strategy.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import victor.bluerock.lightsout.domain.Sample;
import victor.bluerock.lightsout.domain.Solution;
import victor.bluerock.lightsout.loader.SampleFileLoader;

import static org.junit.jupiter.api.Assertions.assertFalse;

class NaiveStrategyTest {

    private NaiveStrategy strategy;

    @BeforeEach
    void before() {
        strategy = new NaiveStrategy();
    }

    @Test
    void findSolution_sample_success() {
        SampleFileLoader loader = new SampleFileLoader();
        Sample sample = loader.loadFile("01.txt");

        Solution solution = strategy.findSolution(sample);
        assertFalse(solution.getCoordinates().isEmpty());
    }
}
