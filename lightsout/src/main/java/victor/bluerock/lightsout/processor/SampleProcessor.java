package victor.bluerock.lightsout.processor;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import victor.bluerock.lightsout.domain.Coordinate;
import victor.bluerock.lightsout.domain.Sample;
import victor.bluerock.lightsout.domain.Solution;
import victor.bluerock.lightsout.loader.SampleFileLoader;
import victor.bluerock.lightsout.strategy.LightsOutStrategyFactory;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Class responsible for the process of sample files.
 */
@Slf4j
@AllArgsConstructor
@Builder
public class SampleProcessor {

    private final SampleFileLoader loader;
    private final LightsOutStrategyFactory factory;

    /**
     * Process sample files.
     *
     * @param sampleFile Sample file that should be processed. If <code>null</code>, process all sample files.
     */
    public void process(String sampleFile) {
        // Retrieve sample.
        List<Sample> samples = Objects.isNull(sampleFile)
                ? loader.loadFiles()
                : Collections.singletonList(loader.loadFile(sampleFile));

        // Find solutions.
        samples.stream()
                .map(this::findSolution)
                .filter(Objects::nonNull)
                .forEach(this::printSolution);
    }

    private Solution findSolution(Sample sample) {
        return factory.createStrategies()
                .stream()
                .map(strategy -> strategy.findSolution(sample))
                .filter(Objects::nonNull)
                .findFirst()
                .orElse(null);
    }

    private void printSolution(Solution solution) {
        log.info("Printing out solution to sample {}.", solution.getSampleFile());

        String result = solution.getCoordinates()
                .stream()
                .map(this::printCoordinate)
                .collect(Collectors.joining());

        log.info(result);
    }

    private String printCoordinate(Coordinate coordinate) {
        return Integer.toString(coordinate.getX())
                .concat(",")
                .concat(Integer.toString(coordinate.getY()))
                .concat(" ");
    }
}
