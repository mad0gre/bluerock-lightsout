package victor.bluerock.lightsout;

import lombok.extern.slf4j.Slf4j;
import victor.bluerock.lightsout.loader.SampleFileLoader;
import victor.bluerock.lightsout.processor.SampleProcessor;
import victor.bluerock.lightsout.strategy.LightsOutStrategyFactory;

/**
 * Main entry point of Lights Out application.
 */
@Slf4j
public class LightsOut {

    /**
     * Main method of Lights Out application.
     *
     * @param args Sample file to be processed may be passed as the first argument.
     */
    public static void main(String[] args) {
        // Retrieve sample file from args.
        String sample = null;
        if (args.length > 0) {
            sample = args[0];
        }

        // Process sample.
        SampleProcessor.builder()
                .loader(new SampleFileLoader())
                .factory(new LightsOutStrategyFactory())
                .build()
                .process(sample);
    }

}
