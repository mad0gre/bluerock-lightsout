package victor.bluerock.lightsout.strategy;

import victor.bluerock.lightsout.strategy.impl.NaiveStrategy;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;



/**
 * Factory class used to generate solutions.
 */
public class LightsOutStrategyFactory {

    /**
     * Creates all strategies in the order they should be attempted.
     *
     * @return List of {@link LightsOutStrategy} implementations.
     */
    public List<LightsOutStrategy> createStrategies() {
        return Stream
                .of(new NaiveStrategy())
                .collect(Collectors.toList());
    }

}
