package victor.bluerock.lightsout.strategy.impl;

import lombok.extern.slf4j.Slf4j;
import victor.bluerock.lightsout.domain.Coordinate;
import victor.bluerock.lightsout.domain.Sample;
import victor.bluerock.lightsout.domain.Solution;
import victor.bluerock.lightsout.strategy.LightsOutStrategy;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Naive strategy to solve a puzzle sample.
 */
@Slf4j
public class NaiveStrategy implements LightsOutStrategy {

    @Override
    public Solution findSolution(Sample sample) {
        log.info("Executing naive strategy.");

        long time = System.currentTimeMillis();

        List<Coordinate> coordinates = placePieces(sample.getPuzzle(), new LinkedList<>(sample.getPieces()), sample.getDepth());
        Solution solution = Solution
                .builder()
                .sampleFile(sample.getSampleFile())
                .coordinates(coordinates)
                .build();

        time = System.currentTimeMillis() - time;
        log.info("Naive strategy took {}ms. Solution found? {}.", time, Objects.nonNull(solution));

        return solution;
    }

    private List<Coordinate> placePieces(int[][] puzzle, List<int[][]> pieces, int depth) {
        if (Objects.nonNull(pieces)
                && !pieces.isEmpty()
                && isPuzzleSolvable(puzzle, depth, pieces.size())) {
            // Bite head.
            int[][] piece = pieces.remove(0);

            // Process body.
            for (int i = 0; i <= puzzle.length - piece.length; i++) {
                for (int j = 0; j <= puzzle[0].length - piece[0].length; j++) {
                    // Put piece in current coordinate.
                    addPiece(puzzle, piece, depth, i, j);

                    // Check if we have a solution
                    if (pieces.isEmpty()) {
                        if (isPuzzleSolved(puzzle)) {
                            // No more pieces. If the puzzle is zeroed out we found a solution.
                            Coordinate coordinate = Coordinate.builder()
                                    .x(i)
                                    .y(j)
                                    .build();

                            pieces.add(0, piece);
                            return Stream.of(coordinate).collect(Collectors.toCollection(() -> new LinkedList<>()));
                        } else {
                            // Puzzle not solved. Remove current piece from puzzle and keep trying.
                            removePiece(puzzle, piece, depth, i, j);
                        }
                    } else {
                        // Try putting next piece in puzzle.
                        List<Coordinate> currentSolution = placePieces(puzzle, pieces, depth);

                        if (currentSolution.isEmpty()) {
                            // No solution as of now. Remove current piece from puzzle and keep trying.
                            removePiece(puzzle, piece, depth, i, j);
                        } else {
                            // Found a solution. Put piece back in list. Add coordinate to response.
                            pieces.add(0, piece);
                            Coordinate coordinate = Coordinate.builder()
                                    .x(i)
                                    .y(j)
                                    .build();
                            currentSolution.add(0, coordinate);
                            return currentSolution;
                        }
                    }
                }
            }

            // Add piece back to list.
            pieces.add(0, piece);
        }

        // No solution found.
        return Collections.emptyList();
    }

    private void addPiece(int[][] puzzle, int[][] piece, int depth, int x, int y) {
        for (int i = 0; i < piece.length; i++) {
            for (int j = 0; j < piece[0].length; j++) {
                puzzle[i + x][j + y] += piece[i][j];
                if (puzzle[i + x][j + y] >= depth) {
                    puzzle[i + x][j + y] = 0;
                }
            }
        }
    }

    private void removePiece(int[][] puzzle, int[][] piece, int depth, int x, int y) {
        for (int i = 0; i < piece.length; i++) {
            for (int j = 0; j < piece[0].length; j++) {
                puzzle[i + x][j + y] -= piece[i][j];
                if (puzzle[i + x][j + y] < 0) {
                    puzzle[i + x][j + y] += depth;
                }
            }
        }
    }

    private boolean isPuzzleSolved(int[][] puzzle) {
        for (int i = 0; i < puzzle.length; i++) {
            for (int j = 0; j < puzzle[0].length; j++) {
                if (puzzle[i][j] != 0) {
                    return false;
                }
            }
        }

        return true;
    }

    private boolean isPuzzleSolvable(int[][] puzzle, int depth, int remaining) {
        for (int i = 0; i < puzzle.length; i++) {
            for (int j = 0; j < puzzle[0].length; j++) {
                if (puzzle[i][j] > 0
                        && depth - puzzle[i][j] > remaining) {
                    return false;
                }
            }
        }

        return true;
    }
}
