package victor.bluerock.lightsout.strategy;

import victor.bluerock.lightsout.domain.Sample;
import victor.bluerock.lightsout.domain.Solution;

/**
 * Interface defining methods to solve a Lights Out puzzle sample.
 */
public interface LightsOutStrategy {

    /**
     * Finds solution for a given puzzle.
     *
     * @param sample Puzzle sample to be solved.
     * @return {@link Solution} containing of coordinates for the pieces that solve the puzzle.
     */
    Solution findSolution(Sample sample);

}
