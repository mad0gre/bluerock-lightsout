package victor.bluerock.lightsout.loader;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;
import victor.bluerock.lightsout.domain.Sample;
import victor.bluerock.lightsout.exception.LightsOutException;

/**
 * Loader used to convert sample files into {@link Sample} objects.
 */
@Slf4j
public class SampleFileLoader {

    private static final String SAMPLE_PATH = "samples";

    private static final String PIECES_SEPARATOR = " ";
    private static final String ROW_SEPARATOR = ",";

    /**
     * Reads all sample files available in the resource folder, and loads samples from them.
     *
     * @return {@link List} of {@link Sample} objects.
     */
    public List<Sample> loadFiles() {
        try {
            URL url = ClassLoader.getSystemResource(SAMPLE_PATH);
            if (Objects.nonNull(url)) {
                return Files.list(Paths.get(url.toURI()))
                        .map(this::loadFile)
                        .collect(Collectors.toList());
            } else {
                log.info("Could not find resource path {}.", SAMPLE_PATH);
                return Collections.emptyList();
            }
        } catch (URISyntaxException e) {
            log.error("Invalid URI syntax when loading files from {}.", SAMPLE_PATH, e);
            throw new LightsOutException(e);
        } catch (IOException e) {
            log.error("Error when listing files from {}.", SAMPLE_PATH, e);
            throw new LightsOutException(e);
        }
    }

    /**
     * Reads a single sample file from the resource folder, and loads a sample from it.
     *
     * @param fileName Sample file name.
     * @return {@link Sample} object.
     */
    public Sample loadFile(String fileName) {
        String fullFileName = String.format(SAMPLE_PATH.concat("/%s"), fileName);
        try {
            URL url = ClassLoader.getSystemResource(fullFileName);
            if (Objects.nonNull(url)) {
                return loadFile(Paths.get(url.toURI()));
            } else {
                log.info("Could not find sample file {}.", fullFileName);
                return null;
            }
        } catch (URISyntaxException e) {
            log.error("Invalid URI syntax when loading files from {}.", SAMPLE_PATH, e);
            throw new LightsOutException(e);
        }
    }

    private Sample loadFile(Path path) {
        try {
            log.info("Loading file {}.", path.toString());

            List<String> lines = Files.readAllLines(path);

            return Sample.builder()
                    .sampleFile(path.getFileName().toString())
                    .depth(Integer.parseInt(lines.get(0)))
                    .puzzle(parsePuzzle(lines.get(1)))
                    .pieces(parsePieces(lines.get(2)))
                    .build();
        } catch (IOException e) {
            log.error("Error when reading contents of file {}.", path.toString(), e);
            throw new LightsOutException(e);
        }
    }

    private int[][] parsePuzzle(String line) {
        String[] rows = line.split(ROW_SEPARATOR);

        int[][] puzzle = new int[rows.length][rows[0].length()];
        for (int i = 0; i < puzzle.length; i++) {
            for (int j = 0; j < puzzle[0].length; j++) {
                puzzle[i][j] = rows[i].charAt(j) - '0';
            }
        }

        return puzzle;
    }

    private List<int[][]> parsePieces(String line) {
        return Arrays
                .stream(line.split(PIECES_SEPARATOR))
                .map(this::parsePiece)
                .collect(Collectors.toList());
    }

    private int[][] parsePiece(String piece) {
        String[] rows = piece.split(ROW_SEPARATOR);

        int[][] result = new int[rows.length][rows[0].length()];
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[0].length; j++) {
                result[i][j] = rows[i].charAt(j) == 'X' ? 1 : 0;
            }
        }

        return result;
    }
}
