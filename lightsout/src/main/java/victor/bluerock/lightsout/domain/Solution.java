package victor.bluerock.lightsout.domain;

import lombok.Builder;
import lombok.Value;

import java.util.List;

/**
 * Model of solution to a sample puzzle.
 */
@Value
@Builder
public class Solution {

    private String sampleFile;
    private List<Coordinate> coordinates;

}
