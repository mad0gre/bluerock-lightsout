package victor.bluerock.lightsout.domain;

import lombok.Builder;
import lombok.Value;

import java.util.List;

/**
 * Model of a puzzle sample to be solved.
 */
@Value
@Builder
public class Sample {

    private String sampleFile;

    private int depth;

    private int[][] puzzle;

    private List<int[][]> pieces;

}
