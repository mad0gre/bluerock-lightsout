package victor.bluerock.lightsout.domain;

import lombok.Builder;
import lombok.Value;

/**
 * Model of coordinate for response.
 */
@Value
@Builder
public class Coordinate {

    private int x;
    private int y;

}
