package victor.bluerock.lightsout.exception;

/**
 * This exception is used only to encapsulate other known exceptions. If this is thrown, it means that the causing
 * error was already logged elsewhere.
 */
public class LightsOutException extends RuntimeException {

    /**
     * Creates a new exception by encapsulating the cause.
     *
     * @param cause {@link Throwable} that caused this exception to be thrown.
     */
    public LightsOutException(Throwable cause) {
        super(cause);
    }
}
